import 'package:firebase_auth/firebase_auth.dart';
import 'package:livingcity/app/interfaces/auth_repository_interface.dart';
import 'package:livingcity/app/repositories/auth/auth_repository.dart';
import 'package:mobx/mobx.dart';

part 'auth_service.g.dart';

class AuthService = _AuthServiceBase with _$AuthService;

abstract class _AuthServiceBase with Store {
  final IAuthRepository _authRepository = AuthRepository();

  @observable
  AuthStatus status = AuthStatus.loading;

  @observable
  FirebaseUser user;

  @action
  setUser(FirebaseUser value) {
    user = value;
    status = user == null ? AuthStatus.loggedOut : AuthStatus.loggedIn;
  }

  _AuthServiceBase() {
    _authRepository.getUser().then(setUser).catchError((e) {
      print('ERRORRRRRR');
    });
  }

  @action
  Future googleSignIn() async {
    user = await _authRepository.googleSignIn();
  }

  @action
  Future signOut() async {
    return _authRepository.signOut();
  }
}

enum AuthStatus { loading, loggedIn, loggedOut }
