import 'package:livingcity/app/services/auth/auth_service.dart';
import 'package:mobx/mobx.dart';
part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  AuthService authService = AuthService();

  @action
  Future signOut() async {
    try {
      await authService.signOut();
    } catch (e) {
      print('error sign out: $e');
    }
  }
}
